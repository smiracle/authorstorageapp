module.exports = {
	authors:
		[
			{
				id: 'steve-miracle',
				firstName: 'Steve',
				lastName: 'Miracle'
			},
			{
				id: 'scott-allen',
				firstName: 'Scott',
				lastName: 'Allen'
			},
			{
				id: 'dan-wahlin',
				firstName: 'Dan',
				lastName: 'Wahlin'
			}
		]
};